const express = require("express");
const router = express.Router();
const courseController = require("../controller/courseController");
const auth = require('../auth')

//Route for create a course
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	if(userData.isAdmin === true){
	courseController.addCourse(req.body).then(result => res.send(result))

} else {
	res.send("NOT ADMIN")
}
})

// Route for retrieving all courses
router.get('/all', auth.verify, (req, res) => {
	courseController.getAllCourses().then(result => res.send(result))
})

// Route for retrieving active courses
router.get('/', (req, res) => {
	courseController.getAllActive().then(result => res.send(result))
})

// Route for retrieving a specific course
router.get('/:courseId', (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(result => {
		res.send(result)
	})
})

// Route for updating a course
router.put('/:courseId', auth.verify, (req, res) => {
	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}

	courseController.updateCourse(data).then(result => res.send(result))
})

// Route for archiving a course
router.put('/:courseId/archive', auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization).isAdmin
	console.log(data)

	courseController.archiveCourse(req.params, data).then(result => res.send(result))
})


module.exports = router;
