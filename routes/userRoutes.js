const express = require('express');
const router = express.Router();
const userController = require('../controller/userController');
const auth = require('../auth')

// Route for Checking if the email exist
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body)
	.then(result => {
		res.send(result)
	})
})

// Route for Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body)
	.then(result => {
		res.send(result)
	})
})

// Routes for User Authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body)
	.then (result => {
		res.send(result)
	})
})

// Routes for Getting a specific user
// auth.verify will act as a middleware to ensure that the user is logged in before they can get the details
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	
	userController.getProfile({userId: userData.id})
	.then((result, err) => {
		if (err){
			return false;
		} else {
		res.send(result);
	}
	})
})

// Route for enrollment
router.post('/enroll', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}

	if(userData.isAdmin === false){
		userController.enroll(data).then(result => {
			res.send(result)
		})
	} else {
		res.send('Not authorized')
	}

})

module.exports = router;