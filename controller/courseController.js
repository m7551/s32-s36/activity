const Course = require("../models/Course");
const router = require("../routes/userRoutes");

// Add a course (Admin privileged)
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	})
}

// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

// Retrieve a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

// Updating a course
module.exports.updateCourse = (data) => {
	console.log(data)

	return Course.findById(data.courseId).then((result, err) => {
		console.log(result)

		if (data.isAdmin){
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price

			console.log(result)
			return result.save().then((updatedCourse, err) => {
				if (err){
					return false
				} else {
					return updatedCourse;
				}
			})
		} else {
			return "Not Admin"
		}
	})
}

// Archiving a course
module.exports.archiveCourse = (reqParams, data) => {
	console.log(data)

	return Course.findById(reqParams.courseId).then((result, err) => {
		console.log(result)
		if(data){
			result.isActive = false
			
			return result.save().then((archivedCourse, err)=> {
				if (err){
					return false
				} else {
					console.log(archivedCourse)
					return true
				}
			})
		} else {
			console.log(data)
			return "Not admin"
		}
	})
}