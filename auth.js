const jwt = require('jsonwebtoken');

const secret = 'CourseBookingAPI';

// Token Creation
module.exports.createAccessToken = (user) => {

	// When the user login, the token will be created with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a JSON web token using the jwt's sign method
	// generates the token using the form data and the secret code with no additional options
	return jwt.sign(data, secret, {});
}

// Verifying the token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=> {
			if (err){
				return res.send({auth: "failed"})
			} else {
				next(); // next() will call the next middlware
			}
		})
	} else {
		return res.send({auth:"failed"})
	}
}

module.exports.decode = (token) => {
	if (typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null;
	}
}