const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

// Allows our backend application to be available to our frontend application
// Allow us to contorl the app's Cross Origin Resource Sharing settings
const cors = require('cors');

// Will use the defined port number for the application whenever an environment variable is available OR will use port 4000 if none is defined
// Allows the flexibility when using the application locally or as a hosted application
const port = process.env.PORT || 4000;

const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin123@course-booking.jmriv.mongodb.net/s32-s36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', () => console.error.bind(console,'Connection Error'));
db.once('open', () => console.log(`MongoDB Database is now established.`))


app.use(cors()); // Allows all resources to access our backend application
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

app.listen(port, () => console.log(`API is now running on port ${port}`));